To run this programm take the following steps:

- run 'mvn clean package' from the root folder (altimatask)
- move to target folder and run 'java -jar crossword-0.0.1-SNAPSHOT.jar'
- follow the instructions on the console
