package net.rustam.crossword;

/**
 * @author Rustam Galiullin
 */
public class Index
{
    private int row;
    private int col;

    Index(int row, int col)
    {
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        { return true; }
        if (o == null || getClass() != o.getClass())
        { return false; }

        Index index = (Index) o;

        if (row != index.row)
        { return false; }
        return col == index.col;
    }

    @Override
    public int hashCode()
    {
        int result = row;
        result = 31 * result + col;
        return result;
    }

    public int getRow()
    {
        return row;
    }

    public int getCol()
    {
        return col;
    }
}
