package net.rustam.crossword;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author Rustam Galiullin
 */
@Component
public class CrosswordFinder
{
    private static final int MIN_WORD_LEN = 3;
    private Set<String> allMatrixWords = new HashSet<>();
    private Map<String, String> dictionary;
    private int max_word_len;
    private int matrixSize;

    public CrosswordFinder()
    {
        this.dictionary = createDictionary();
        this.max_word_len = findMaxWordLen();
    }

    List<String> find(char[][] matrix, int matrixSize)
    {
        this.matrixSize = matrixSize;

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                createWords(matrix, new Index(i, j), new HashSet<>(), new StringBuilder(), MIN_WORD_LEN, max_word_len);
            }
        }

        List<String> foundWords = new ArrayList<>();

        for (String matrixWord : allMatrixWords)
        {
            String dictionaryWord = dictionary.get(matrixWord);
            if (dictionaryWord != null)
            {
                foundWords.add(dictionaryWord);
            }
        }
        return foundWords;
    }

    private void createWords(
            final char[][] matrix,
            final Index startIndex,
            final Set<Index> members,
            final StringBuilder word,
            final int minWordLength,
            final int maxWordLength)
    {
        word.append(matrix[startIndex.getRow()][startIndex.getCol()]);

        if (word.length() >= minWordLength)
        {
            allMatrixWords.add(word.toString());
        }
        if (word.length() == maxWordLength)
        {
            word.deleteCharAt(word.length() - 1);
            return;
        }

        Set<Index> newMembers = findMembers(startIndex, matrixSize);
        members.addAll(newMembers);

        for (Index index : members)
        {
            Set<Index> membersCopy = new HashSet<>(members);
            StringBuilder wordCopy = new StringBuilder(word);
            createWords(matrix, index, membersCopy, wordCopy, minWordLength, maxWordLength);
        }
    }

    private Set<Index> findMembers(final Index index, int matrixSize)
    {
        Set<Index> members = new HashSet<>();

        List<Integer> rows = new ArrayList<>();
        List<Integer> cols = new ArrayList<>();

        int row1 = index.getRow() - 1;
        if (row1 > -1 && row1 < matrixSize)
        { rows.add(row1); }

        int row2 = index.getRow();
        if (row2 > -1 && row2 < matrixSize)
        { rows.add(row2); }

        int row3 = index.getRow() + 1;
        if (row3 > -1 && row3 < matrixSize)
        { rows.add(row3); }

        int col1 = index.getCol() - 1;
        if (col1 > -1 && col1 < matrixSize)
        { cols.add(col1); }

        int col2 = index.getCol();
        if (col2 > -1 && col2 < matrixSize)
        { cols.add(col2); }

        int col3 = index.getCol() + 1;
        if (col3 > -1 && col3 < matrixSize)
        { cols.add(col3); }

        for (int row : rows)
        {
            for (int col : cols)
            {
                Index newIndex = new Index(row, col);
                members.add(newIndex);
            }
        }
        return members;
    }

    private Map<String, String> createDictionary()
    {
        Map<String, String> dictionarySet = new HashMap<>();
        try
        {
            InputStream inputStream = new ClassPathResource("wordlist.txt").getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null)
            {
                dictionarySet.put(line, line);
            }
        }
        catch (IOException e)
        {
            System.err.println(e.fillInStackTrace());
        }
        return dictionarySet;
    }

    private int findMaxWordLen()
    {
        // TODO: find the longest word in dictionary.
        return 7;
    }
}
