package net.rustam.crossword;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

/**
 * @author Rustam Galiullin
 */
@Component
public class CrosswordCommandLineRunner implements CommandLineRunner
{
    private final CrosswordFinder crosswordFinder;

    @Autowired
    public CrosswordCommandLineRunner(CrosswordFinder crosswordFinder)
    {
        this.crosswordFinder = crosswordFinder;
    }

    @Override
    public void run(String... args) throws Exception
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the value of n:\n");
        String nValue = scanner.next();
        int matrixSize = new Integer(nValue);
        System.out.print("Enter the content of " + matrixSize + "x" + matrixSize + " sized letter-matrix row by row:\n");

        char[][] matrix = new char[matrixSize][];

        for (int i = 0; i < matrixSize; i++)
        {
            String row = scanner.next();
            matrix[i] = row.toCharArray();
        }
        System.out.print("Please wait ... \n");

        List<String> foundWords = crosswordFinder.find(matrix, matrixSize);
        foundWords.forEach(System.out::println);
    }

}
